\section{Definitions}\label{sec:definitions}\contents{we give detailed descriptions of the elements that will be discussed in the remainder of the paper}

We consider in this section the elements in the specification of a OBDA system, the changes that can be performed to them and the desired properties of a OBDA system.

\subsection{Elements in the specification}

We have seen that an ontology $\ontology$ is divided in a TBox $\tbox$ and an ABox $\abox$, corresponding to the pair $\tup{\tbox, \abox}$.
An OBDA system works as an abstraction of an ontology and can thus be seen as an ontology, in this case pair $\obda = \tup{\tboxd, \aboxd}$.
The TBox $\tboxd$ is usually a regular TBox, in a description logic (DL) language that displays a good computational behaviour for query answering to allow the access to the data, e.g. DL-Lite, OWL 2 QL and Datalog$\pm$.
Normally this good computational behaviour is captured by the notion of first-order rewritability~\cite{calvanese2007}, meaning that the queries over the OBDA system $\obda$ as an ontology $\ontology$ can be rewritten to first order queries over the data sources, usually in languages like SQL.
The ABox $\aboxd$ in an OBDA system is an abstraction provided by a set of mappings $\mappings$ and the data sources $\datas$, thus $\aboxd = \tup{\mappings, \data}$
For example, some OBDA systems in the past focused exclusively on the ABox without considering the TBox. 
In these cases the system would populate the ABox, materialize it in RDF or provide an interface to query for the assertions in it.
The mappings $\mappings$ relate the entities in the TBox $\tboxd$ with the entities in the data sources $\datas$.
This is done through assertions of the form: $ \mappingassertion$.

Where $\body[\variables]$ is a query over the signature of $\datas$ ($\signature[\datas]$), $\head[\variables]$ is a query over the signature of $\tboxd$ ($\signature[\tboxd]$) and the signature of some element is the set of entities or terms in that element $\element$, $\signature[\element]$.
The data sources are normally databases, which means that the mappings relate the entities in the TBox with those in the database schemata $\schemas$.
These schemata may impose some integrity constraints over the data that is stored in these databases.
We say that a database $\data$ is legal for a schema $\schema$ if $\data$ satisfies the constraints imposed by $\schema$.
Specifically for the access to relational databases, languages like the W3C standard R2RML~\cite{das2012} are of common use.

\subsection{Changes in OBDA systems}

The ways in which some element in an OBDA specification can change are, in the most basic description, simply three: addition, removal and modification.
%We can consider that modification is a removal plus an addition, but grouping together these two operations provides a different context for the analysis.
%For example, considering the modification as two operations (addition and removal), we may find that the consistency of the OBDA system is lost between one operation and the other, but regained after the whole modification.
%This kind of behaviour is not exclusive to the modification of some element but it may also happen when grouping some other changes.
%For example, 
A modification can be considered as a removal and an addition, but it happens at a different granularity level.
For example, when the body of a mapping is modified, we can consider that we delete the former mapping and add a new one.
However, having the information about the relation between both mappings (as the modification of a mapping) may help to detect possible actions to propagate this change, e.g. the modification on the head of the mapping.
The modification of atomic elements is effectively a removal and an addition, being atomic there is no part preserved in the modification of the only part that composes the atomic element.
Therefore no further considerations need to be made about the modification of atomic elements.
The practical relevance of this considerations strives in how the changes should be analysed.



\subsection{Changes in the elements}

The possible atomic changes in a OBDA specification can be classified depending on the element in the OBDA specification over which they are performed.

\subsubsection{Changes in the ontology}

Most elements in the ontology are atomic except for some axioms that relate sets of elements (e.g. sets of disjoint classes or different individuals).
The operations to perform on the elements in the ontology are therefore addition, removal and modification (of these axioms).
For the propagation of the changes, we can distinguish two different parts in the ontology: the set of axioms and the set of declarations (the signature).
The types axioms that can be present in an ontology define the expressiveness of the ontology.
The elements in the signature can be classified into three types: concepts, roles and attributes.
The consequences of the changes in both sets of elements are different.
Changes in axioms will lead to consistency checks and checking the semantic implications of these axioms.
Changes in the signature will lead to repairing references to these elements, when deleting elements some references will be broken, when adding elements references to them are enabled and may be checked.

\subsubsection{Changes in the data source schemas}\label{sec:source-schema-changes}

Elements in the data source schemas are tables, columns and constraints.
In this case tables are not atomic, however the modification of tables is already captured by the addition and removal of columns and constraints, therefore no considerations need to be made about the modification of tables.
Similarly columns may be subject of several constraints, but the addition and removal of constraints is captured independently, therefore no additional considerations about the modifications of columns are necessary.
Finally constraints are atomic and therefore they cannot be modified, but added and removed.

Similarly to the changes in ontologies, the changes in tables and columns modify the signature of the database, which has immediate implications on the mappings that may depend on the signature elements.
The changes in this case need to be propagated for syntactic reasons and may have semantic implications.
Changes in the constraints of the constraints of the database imply different semantics that need to be analysed further to keep the OBDA system in a consistent and coherent state.
The changes in this case need to be propagated for semantic reasons and may have syntactic implications.


\subsubsection{Changes in the mappings}

The mappings relate the ontology with the data sources, which means that their signature is potentially the union of the signatures of both elements.
Changes in the mappings contain the union of the changes in the signatures of ontologies and data sources and additionally the relation between both parts of the OBDA system.
More precisely a mapping can be added, deleted or modified by modifying its head or its body.
In the modification of the head we find the addition or removal of concepts, roles and attributes.
In the case of mapping languages like R2RML we can additionally consider the modification of the templates for the URIs in the head of the mapping, which means changing the constant or the variable part, with the latter having more implications on the semantics of the mapping (e.g. modification of the distinguished variables of the mapping).

In the modification of the body we find the modification of the query that may be specified in SQL or using a different declarative syntax (with equivalent semantics).
In either case, several changes can be produced in the body of the mapping, such as: the distinguished variables in the query, the tables and columns from which the information is obtained, the filtering conditions.

\subsubsection{Changes in the data}

The data is the most dynamic element and OBDA systems usually consider the possibility of great dynamism in the data sources.
New statements (rows in a database) can be added to the data sources and database management systems are usually prepared for dynamic data.
These systems ensure that the constraints defined in the schema are satisfied by the data.
Changes in the data are mentioned in the sake of exhaustiveness, however they should pose no problem in any reasonable OBDA system.
We can find an exception in systems integrating data sources with no known schema, so that it is obtained from the data (or implicit in it).
This would mean that changes in the data could imply changes in the (tacit) schema of the data.
For systems where that is the case, we refer the reader to the casuistry aforementioned in section \ref{sec:source-schema-changes}, with the additional consideration that changes to the schema may happen tacitly.





\subsection{Evolution in OBDA systems}

We can view a OBDA system $\obda$ as a first order logic (FOL) theory that derives some assertions (the answers to the queries) from a set of initial assertions (the data in the data sources) using a set of logical formulas (the queries, the mappings and the TBox).
From this point of view we can consider two fundamental properties that a OBDA system should preferably hold, consistency and coherence.

We say that a OBDA system is \emph{consistent} when it implies no contradictions, i.e. $\obda \not\models \bot$.
A system that is not consistent may produce incorrect results.

We say that a OBDA system is \emph{coherent} when it contains no trivial assertions, i.e. $\left(\obda \models \body[{{\variables},{\variablestwo[m]}}] \rightarrow \head[{{\variables},\variablesthree[l]}]\right) \Rightarrow \exists \constants[n+m]. \obda \models \body[{\constants[n+m]}]$.
Coherence is important because a system that is not coherent may contain a latent inconsistency, 

We can additionally consider the recall of OBDA systems. %completeness?
For example, given two OBDA systems over the same data $\obda = \tup{\tbox,\tup{\mappings, \data}}$ and $\obda^\prime = \tup{\tbox^\prime,\tup{\mappings^\prime, \data}}$, we can say that the recall of $\obda$ is greater than the recall of $\obda^\prime$ iff three conditions hold:
\begin{enumerate}
	\item both are consistent.
	\item there is some set of values (constants) $\constants$ and some predicate $\head$ such that $\obda \models \head[\constants] \land \obda^\prime \not\models  \head[\constants]$.
  \item there is no set of values $\constants$ and some predicate $\head$ such that $\obda^\prime \models \head[\constants] \land \obda \not\models  \head[\constants]$.
\end{enumerate}

We can derive from these definitions that the recall of OBDA systems for a given data source (or set of data sources) defines a partially ordered set.

Evolution consists on the maintenance of the consistency and coherency of the OBDA specification through changes performed to it as well as an attempt to improve the recall of the OBDA system when new opportunities are present, adapting to the changes that occur in the context of the OBDA system and propagating these changes through the OBDA system and to dependent artifacts.



